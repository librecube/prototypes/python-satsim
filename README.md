# LibreSim

LibreSim is a simulation modeling platform implemented in Python that follows
the recommendations provided in the ECSS Standard [ECSS-E-ST-40-07C](docs/ECSS-E-ST-40-07C.pdf).

It consists of two parts:
- a kernel to run simulations (based on discrete-event simulation and using [SimPy](https://simpy.readthedocs.io))
- a model library to build your system that is to be simulated (adhering to SMP2 modelling specification)

## Installation

Clone the repository and then install via pip:

```
$ git clone https://gitlab.com/librecube/prototypes/python-libresim
$ cd python-libresim
$ python -m venv venv
$ source venv/bin/activate
(venv) $ pip install -e .
```

## Example

In the examples folder you can find a number of examples to run. For instance,
to run the counter example, do as follows:

```
(venv) $ cd examples
(venv) $ python run.py basic/counter
```

The output will be shown in the terminal. Also, visit http://localhost:5000
to monitor the simulation via the web server.

## Documentation

Read the [detailed documentation](docs/README.md) to learn how to use LibreSim.

Also, there is some (likely outdated) reference documentation in docs/references
that may serve for inspiration and understanding.

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-libresim/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-libresim

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the GPL3 license. See the [LICENSE](./LICENSE.txt) file for details.
