# LibreSim

LibreSim is a software infrastructure to support the development and
running of simulations. Its main focus is on simulation of space missions
but it is not limited to it. The simulation architecture follows the ECSS
Standard [ECSS-E-ST-40-07C](ECSS-E-ST-40-07C.pdf).

ECSS-E-ST-40-07 is a standard published by the [European Cooperation for Space
Standardization](https://ecss.nl/). It is specific to simulation software and
covers both simulation environments and simulation models. The standard enables
the effective reuse of simulation models within and between space projects and
their stakeholders.

The objective of LibreSim is to provide a simulation infrastructure that can
be extended with custom (mission-specific) models to simulate various kinds
of scenarios.

The main purpose of any simulator is to model the actions and reactions of the
real system during operations. The simulator is used during mission preparation
for training, procedure validations and ground segment testing, and during mission
operations to test proposed changes to the system before they are deployed to the
real vehicle.

The simulator interfaces with the mission control system through telecommand
and telemetry links. The simulator is controlled through command line or via
a GUI application (both to be developed).

## Architecture

The simulator architecture consists of:
- a simulation environment that provides base classes and simulation services
- common models that provide the basics for simulations
- generic models as basis for modeling the user specific system to be simulated

![](figures/architecture.png)


# Getting Started

...
