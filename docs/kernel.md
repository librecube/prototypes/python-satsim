# LibreSim Kernel Documentation

The simulation services comprise:

- Logger: Allows any component to log messages
- Scheduler*: Allows calls of entry points based on timed or cyclic events
- Time Keeper*: Provides the four different time kinds
- Event Manager*: Provides mechanisms for global asynchronous events
- Resolver*: Provides the ability to get a reference to any model within a simulation
- Link Registry: Maintains a list of the links between model instances

All information regarding the simulation environment and the basic simulation
models can be found in the ECSS-E-ST-40-07 standard.

### Scheduler

The Scheduler service has the purpose to execute scheduled events. The scheduler
maintains the scheduled events in the from of a dictionary with {key:event}, where
key is an increasing event number and event contains the information about the
event (a reference to the entrypoint, the simulation time to execute the event, etc.)


### TimeKeeper

The TimeKeeper service manages the four different time kinds.

- **Simulation Time**: Relative time since start of simulation. Progresses when simulator is running.

  Example: 0...1...2

- **Zulu Time**: The computer clock time, that is, the current wall clock time.

  Example: 2022-05-13T10:08:11...2022-05-13T10:08:12...2022-05-13T10:08:13 (for real-time progress)

- **Epoch Time**: The calendar time of the simulation. Typically set to months or years in the future.

  Example: 2030-01-01T00:00:00...2030-01-01T00:00:01...2030-01-01T00:00:02

- **Mission Start Time**: A calender time, typical the launch of the mission.

  Example: 2020-08-01T10:08:11

- **Mission Time**: A relative time, calculated as EpochTime - MissionStartTime.

  Example: 327682...327683...327684

Epoch and mission time have fixed offset to simulation time and progress linear with simulation time.






## Logical Structure

### Simulator Class

### Object Class

Serves as base class for almost all other classes. It provides name, description
and parent, which have to be defined at construction time.

### Collection Class

Serves as a generic collection. The collection allows to Add and Remove children,
and to Clear its content. Typically, the Add operation does check for a unique name.
For a few cases where this is not required, the AddNonUnique operation is provided.

### Component Class

Component is the base class for all models and services. It provides state and
state transition methods.

### Model Class

An empty class, derived from Component, to server as basis for all models.

### Service Class

An empty class, derived from Component, to server as basis for all services.
