from libresim import Model, EntryPoint, EntryPointPublisher


class Example(Model, EntryPointPublisher):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        EntryPointPublisher.__init__(self, name, description, parent)

        self.add_entry_point(
            EntryPoint("print_output", "", self, function=self.print_output)
        )

    def connect(self):
        self.get_scheduler().add_simulation_time_event(
            self.get_entry_point("print_output"),
            simulation_time=0,
            cycle_time=1,
            repeat=-1,
        )

    def print_output(self):
        epoch_time = self.get_time_keeper().get_epoch_time()
        mission_start_time = self.get_time_keeper().get_mission_start_time()
        mission_time = self.get_time_keeper().get_mission_time()
        simulation_time = self.get_time_keeper().get_simulation_time()
        zulu_time = self.get_time_keeper().get_zulu_time()

        print(
            f"Epoch: {epoch_time}\n"
            f"Mission Start Time: {mission_start_time}\n"
            f"Mission Time: {mission_time}\n"
            f"Simulation Time: {simulation_time}\n"
            f"Zulu Time: {zulu_time}\n"
            "\n"
        )


root = [Example("Example", "Print the different time kinds")]
