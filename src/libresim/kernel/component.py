import enum

from .object import Object


class InvalidComponentState(Exception):
    pass


class ComponentState(enum.IntEnum):
    CREATED = 0
    PUBLISHING = 1
    CONFIGURED = 2
    CONNECTED = 3
    DISCONNECTED = 4


class Component(Object):
    def __init__(self, name, description="", parent=None):
        Object.__init__(self, name, description, parent)
        self._simulator = None
        self._receiver = None
        self._logger = None
        self._link_registry = None
        self._scheduler = None
        self._time_keeper = None
        self._resolver = None
        self._state = ComponentState.CREATED

    def get_receiver(self):
        return self._receiver

    def get_logger(self):
        return self._logger

    def get_link_registry(self):
        return self._link_registry

    def get_simulator(self):
        return self._simulator

    def get_event_manager(self):
        return self.get_simulator().get_event_manager()

    def get_resolver(self):
        return self.get_simulator().get_resolver()

    def get_scheduler(self):
        return self.get_simulator().get_scheduler()

    def get_time_keeper(self):
        return self.get_simulator().get_time_keeper()

    def get_state(self):
        return self._state

    def _publish(self, receiver):
        if self._state != ComponentState.CREATED:
            raise InvalidComponentState()
        self._receiver = receiver
        self._state = ComponentState.PUBLISHING
        self.publish()

    def _configure(self, logger, link_registry):
        if self._state != ComponentState.PUBLISHING:
            raise InvalidComponentState()
        self._logger = logger
        self._link_registry = link_registry
        self.configure()
        self._state = ComponentState.CONFIGURED

    def _connect(self, simulator):
        if self._state != ComponentState.CONFIGURED:
            raise InvalidComponentState()
        self._simulator = simulator
        self.connect()
        self._state = ComponentState.CONNECTED

    def _disconnect(self):
        if self._state != self.CONNECTED:
            raise InvalidComponentState()
        self.disconnect()
        self._state = ComponentState.DISCONNECTED

    def publish(self):
        """Publish all publishable fields, properties, and operations."""
        # override with custom code...
        pass

    def configure(self):
        """Perform initial configuration of the component."""
        # override with custom code...
        pass

    def connect(self):
        """Connect to the simulator environment and other components."""
        # override with custom code...
        pass

    def disconnect(self):
        """Disconnect from the simulation environment and other components."""
        # override with custom code...
        pass
