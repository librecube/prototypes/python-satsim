import enum

from ..service import Service


class LogMessageKind(enum.Enum):
    INFORMATION = "INFORMATION"
    EVENT = "EVENT"
    WARNING = "WARNING"
    ERROR = "ERROR"
    DEBUG = "DEBUG"


class Logger(Service):
    def __init__(self, name, description="", parent=None):
        Service.__init__(self, name, description, parent)
        self._log_history = []

    def log(self, sender, message, kind=None):
        log_entry = f"{kind} | {sender.get_name()} | {message}"
        print(log_entry)
        self._log_history.append(log_entry)

    def log_info(self, sender, message):
        self.log(sender, message, kind=LogMessageKind.INFORMATION)

    def log_event(self, sender, message):
        self.log(sender, message, kind=LogMessageKind.EVENT)

    def log_warning(self, sender, message):
        self.log(sender, message, kind=LogMessageKind.WARNING)

    def log_error(self, sender, message):
        self.log(sender, message, kind=LogMessageKind.ERROR)

    def log_debug(self, sender, message):
        self.log(sender, message, kind=LogMessageKind.DEBUG)
