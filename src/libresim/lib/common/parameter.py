from libresim import Model, Composite, Container, SimpleField


class Parameter(Model):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)

        self._value = None
        self.raw_value = SimpleField(
            "raw_value", "Raw value of the parameter", self
        )
        self.calibrated_value = SimpleField(
            "calibrated_value", "Calibrated value of the parameter", self
        )

    def publish(self):
        self._receiver.publish_field(self.raw_value)
        self._receiver.publish_field(self.calibrated_value)

    def set_value(self, value):
        self._value = value

    def get_value(self):
        return self._value

    def set_raw_value(self, raw_value):
        raise NotImplementedError

    def set_calibrated_value(self, calibrated_value):
        raise NotImplementedError


class ParameterPool(Model, Composite):
    def __init__(self, name, description="", parent=None):
        Model.__init__(self, name, description, parent)
        Composite.__init__(self, name, description, parent)

        self.add_container(Container("Parameters"))

    def add_parameter(self, parameter):
        self.get_container("Parameters").add_component(parameter)

    def get_parameter(self, parameter_name):
        for parameter in self.get_container("Parameters").get_components():
            if parameter.get_name() == parameter_name:
                return parameter
