import time

from libresim import Aggregate, Reference, Model
from .utils import setup_simulator


def test_aggregate():
    model = Model("model")
    reference = Reference("reference")
    reference.add_component(model)
    aggregate = Aggregate("aggregate")
    aggregate.add_reference(reference)

    root_models = [model, aggregate]
    simulator = setup_simulator(root_models)
    simulator.run()
    time.sleep(0.1)
    simulator.hold()
    simulator.exit()
